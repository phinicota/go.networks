# go.networks

This library employs the [lumped element model](https://en.wikipedia.org/wiki/Lumped_element_model) and [network analysis](https://en.wikipedia.org/wiki/Network_analysis_(electrical_circuits) to solve the state of a collection of interconnected components.

Easier: it uses the [analogical model](https://en.wikipedia.org/wiki/Analogical_models) of [electrical impedance](https://en.wikipedia.org/wiki/Electrical_impedance) in other *target systems* (like [thermal](https://en.wikipedia.org/wiki/Thermal_resistance#Problems_with_electrical_resistance_analogy)) to simplify the resolution of a physical system state in a finite space and time model.
