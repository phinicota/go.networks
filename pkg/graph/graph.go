package graph

import (
	"bytes"
	"fmt"
)

type INode interface {
	In() Edges
	Out() Edges
	AddIn(e *Edge)
	AddOut(e *Edge)
}

type Node struct {
	in, out Edges
	Name    string
}

func NewNode(name string) *Node {
	return &Node{
		in:   Edges{},
		out:  Edges{},
		Name: name,
	}
}

func (n *Node) String() string {
	return n.Name
}

func (n *Node) In() Edges {
	return n.in
}

func (n *Node) Out() Edges {
	return n.out
}

func (n *Node) AddIn(e *Edge) {
	n.in = append(n.in, e)
}

func (n *Node) AddOut(e *Edge) {
	n.out = append(n.out, e)
}

var _ INode = &Node{}

type IEdge interface {
	From(n *Node) *Edge
	To(n *Node) *Edge
}

type Edge struct {
	from, to *Node
}

func (e *Edge) String() string {
	return fmt.Sprintf("%v -> %v",
		e.from, e.to)
}

func (e *Edge) To(n *Node) *Edge {
	e.to = n
	return e
}

func (e *Edge) From(n *Node) *Edge {
	e.from = n
	return e
}

var _ IEdge = &Edge{}

type Nodes []*Node
type Edges []*Edge

// Graph represents a collection of nodes
// interconnected by edges
// https://en.wikipedia.org/wiki/Graph_(abstract_data_type)
type Graph struct {
	Nodes
	Edges
}

func NewGraph() *Graph {
	return &Graph{
		Nodes: Nodes{},
		Edges: Edges{},
	}
}

func (g *Graph) AddNode(node *Node) {
	g.Nodes = append(g.Nodes, node)
}

// Satisfies Stringer
func (g *Graph) String() string {
	var b bytes.Buffer
	for _, node := range g.Nodes {
		// ins
		for _, edge := range node.In() {
			b.WriteString(edge.String() + "\n")
		}

		// outs
		for _, edge := range node.Out() {
			b.WriteString(edge.String() + "\n")
		}
	}
	return b.String()
}
func (g *Graph) AddEdge(edge *Edge) {
	g.Edges = append(g.Edges, edge)
}
