package networks

import "gitlab.com/phinicota/go.networks/pkg/graph"

// Network represents a collection of interconnected
// components
// https://en.wikipedia.org/wiki/Network_analysis_(electrical_circuits)
type Network struct {
	*graph.Graph
}

func NewNetwork() *Network {
	return &Network{
		Graph: graph.NewGraph(),
	}
}
