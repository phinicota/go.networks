package networks

// Resistance exposes the real part of a complex impedance
// It represents the resistive component of an analogic model
// i.e.,: the resistance of an electrical impedance, of a thermal impedance, etc
type Resistive interface {
	R() float64
}

// Capacitance exposes the imaginary part of a complex impedance
// It represents the reactive component of an analogic model
// i.e.: the inductance or capacitance of an electrical impedance,
// of a thermal element (thermal mass), etc
type Capacitance interface {
	X() float64
}
