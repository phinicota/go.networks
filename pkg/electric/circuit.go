package electric

import (
	"gitlab.com/phinicota/go.networks/pkg/networks"
)

type Circuit struct {
	*networks.Network
}

func NewCircuit() *Circuit {
	return &Circuit{
		Network: networks.NewNetwork(),
	}
}
